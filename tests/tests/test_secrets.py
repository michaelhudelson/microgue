from microgue.secrets.secrets import Secrets
from unittest import TestCase


class TestSecrets(TestCase):
    def test_get_string_secret(self):
        assert Secrets().get("microgue-string-secret-local") == "success"

    def test_get_json_secret(self):
        secret = Secrets().get("microgue-json-secret-local")

        assert secret["username"] == "some_username"
        assert secret["password"] == "some_password"
