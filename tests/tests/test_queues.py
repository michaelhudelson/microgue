from microgue.queues.abstract_queue import AbstractQueue
from unittest import TestCase


class MessageQueue(AbstractQueue):
    queue_url = "https://sqs.us-east-2.amazonaws.com/016032182066/microgue-message-queue-local"


class TestQueues(TestCase):
    def test_send_receive_and_deleted_message(self):
        queue = MessageQueue()

        # clean up queue
        for message in queue.receive():
            queue.delete(message)

        # create new message
        message = {"message": "success"}

        # check send message
        assert queue.send(message)

        messages = queue.receive()

        # check receive message
        assert len(messages) == 1

        # check message content
        assert messages[0].data == message

        # check delete message
        assert queue.delete(messages[0])
