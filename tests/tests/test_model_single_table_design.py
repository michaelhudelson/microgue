import uuid
from datetime import datetime
from microgue.models.attributes import Attribute, DatetimeAttribute, EmptySortKeyAttribute, ObjectAttribute, PartitionKeyAttribute, SortKeyAttribute, UniqueAttribute
from microgue.models.errors import RequiredAttributesMissing, RelatedModelsMissing
from microgue.models.model import BaseTable, Model, Object
from unittest import TestCase


class SingleTable(BaseTable):
    table_name = "microgue-single-table-local"
    indexes = {
        "users-by-department-index": {
            "pk": "pk",
            "sk": "department"
        },
        "users-by-department-and-title-index": {
            "pk": "department",
            "sk": "title"
        }
    }


class ContactInformation(Object):
    email = Attribute(type=str)
    phone_number = Attribute(type=str)


class EmergencyContact(Object):
    full_name = Attribute(type=str)
    contact_information = ObjectAttribute(type=ContactInformation)


class CompanyEmployee(Model):
    class Table(SingleTable):
        pass

    id = SortKeyAttribute(label="EMPLOYEE")
    company_id = PartitionKeyAttribute(label="COMPANY", auto_generate=False)
    full_name = Attribute(type=str)

    emergency_contact = ObjectAttribute(type=EmergencyContact)

    department = Attribute(type=str)
    title = Attribute(type=str)

    created_on = DatetimeAttribute()
    updated_on = DatetimeAttribute()


class CompanyDepartment(Model):
    class Table(SingleTable):
        pass

    id = SortKeyAttribute(label="DEPARTMENT")
    company_id = PartitionKeyAttribute(label="COMPANY", auto_generate=False)
    department_name = Attribute(type=str)


class CompanyProfile(Model):
    class Table(SingleTable):
        pass

    id = PartitionKeyAttribute(label="COMPANY")
    sk = EmptySortKeyAttribute(label="PROFILE")
    company_name = UniqueAttribute()

    related_models = [CompanyEmployee, CompanyDepartment]


class TestModelSingleTableDesign(TestCase):
    def test_create_get_and_get_by_unique_company_profile(self):
        company_profile = CompanyProfile()
        company_profile.company_name = f"Acme Inc. {str(uuid.uuid4())}"

        # check that id is None before save
        assert company_profile.id is None

        company_profile.save()

        # check that the company profile can be re-saved
        try:
            company_profile.save()
            assert bool("Company profile re-saved")
        except:  # noqa
            company_profile.delete()
            assert not bool("Company profile failed to re-save")

        # get company profile by id and sk
        try:
            get_company_profile = CompanyProfile.get(company_profile.id)
        except Exception:
            company_profile.delete()
            assert not bool("Failed to get company profile by id and sk")

        # get company profile by unique attribute
        try:
            by_unique_company_profile = CompanyProfile.get_by_unique_attribute("company_name", company_profile.company_name)
        except Exception:
            company_profile.delete()
            assert not bool("Failed to get company profile by unique attribute")

        # cleanup
        company_profile.delete()

        assert company_profile.id is not None
        assert company_profile.serialize() == get_company_profile.serialize()
        assert company_profile.serialize() == by_unique_company_profile.serialize()

    def test_create_and_get_company_employee(self):
        company_employee = CompanyEmployee()
        company_employee.full_name = "John Doe"
        company_employee.emergency_contact.full_name = "Jane Doe"
        company_employee.emergency_contact.contact_information.email = "fake@email.com"
        company_employee.emergency_contact.contact_information.phone_number = "555-555-5555"

        # check that id is None before save
        assert company_employee.id is None

        # check that company employee does not save without company id
        try:
            company_employee.save()  # should throw exception
            company_employee.delete()  # should not get here
            assert not bool("Company employee saved without company id")
        except RequiredAttributesMissing as e:
            assert str(e) == "missing the following required attributes: company_id"

        # create company profile
        company_profile = CompanyProfile()
        company_profile.company_name = f"Acme Inc. {str(uuid.uuid4())}"
        try:
            company_profile.save()
        except: # noqa
            company_employee.delete()
            assert not bool("Failed to save company profile")

        # assign company id to company employee
        company_employee.company_id = company_profile.id

        # check that the company employee can be created with company id
        try:
            company_employee.save()
            assert bool("Company employee saved with company id")
        except:  # noqa
            company_profile.delete()
            assert not bool("Company employee failed to save with company id")

        # check that the company employee can be re-saved
        try:
            company_employee.save()
            assert bool("Company employee re-saved")
        except:  # noqa
            company_profile.delete()
            company_employee.delete()
            assert not bool("Company employee failed to re-save")

        # get company employee by id and sk
        try:
            get_company_employee = CompanyEmployee.get(company_employee.company_id, company_employee.id)
        except Exception:
            company_employee.delete()
            company_profile.delete()
            assert not bool("Failed to get company employee by id and sk")

        # cleanup
        company_employee.delete()
        company_profile.delete()

        assert isinstance(company_employee.created_on, datetime)
        assert isinstance(company_employee.updated_on, datetime)
        assert company_employee.id is not None
        assert company_employee.serialize() == get_company_employee.serialize()

    def test_get_all_company_employees_by_company_id(self):
        # create company profile
        company_profile = CompanyProfile({
            "company_name": f"Acme Inc. {str(uuid.uuid4())}"
        })
        company_profile.save()

        # create company employees
        company_employee_1 = CompanyEmployee({
            "company_id": company_profile.id,
            "full_name": "John Doe"
        })
        company_employee_1.save()

        company_employee_2 = CompanyEmployee({
            "company_id": company_profile.id,
            "full_name": "Jane Doe"
        })
        company_employee_2.save()

        company_employee_3 = CompanyEmployee({
            "company_id": company_profile.id,
            "full_name": "John Smith"
        })
        company_employee_3.save()

        # create company departments
        company_department_1 = CompanyDepartment({
            "company_id": company_profile.id,
            "department_name": "Sales"
        })
        company_department_1.save()

        company_department_2 = CompanyDepartment({
            "company_id": company_profile.id,
            "department_name": "Marketing"
        })
        company_department_2.save()

        # get all with the object types being the same
        company_profiles = CompanyProfile.get_all(company_profile.id)
        company_employees = CompanyEmployee.get_all(company_profile.id)
        company_departments = CompanyDepartment.get_all(company_profile.id)

        # get all with the object types being related
        get_company_related = CompanyProfile.get_all(company_profile.id, get_related=True)

        # cleanup
        company_profile.delete()
        company_employee_1.delete()
        company_employee_2.delete()
        company_employee_3.delete()
        company_department_1.delete()
        company_department_2.delete()

        assert len(company_profiles) == 1
        assert isinstance(company_profiles[0], CompanyProfile)
        assert len(company_employees) == 3
        assert isinstance(company_employees[0], CompanyEmployee)
        assert len(company_departments) == 2
        assert isinstance(company_departments[0], CompanyDepartment)

        assert len(get_company_related) == 6
        assert isinstance(get_company_related[0], CompanyDepartment)
        assert isinstance(get_company_related[1], CompanyDepartment)
        assert isinstance(get_company_related[2], CompanyEmployee)
        assert isinstance(get_company_related[3], CompanyEmployee)
        assert isinstance(get_company_related[4], CompanyEmployee)
        assert isinstance(get_company_related[5], CompanyProfile)

    def test_base_table_get_all_by_indexes(self):
        # create company profile
        company_profile = CompanyProfile({
            "company_name": f"Acme Inc. {str(uuid.uuid4())}"
        })
        company_profile.save()

        # create company departments
        company_department_1 = CompanyDepartment({
            "company_id": company_profile.id,
            "department_name": "Sales"
        })
        company_department_1.save()

        company_department_2 = CompanyDepartment({
            "company_id": company_profile.id,
            "department_name": "Marketing"
        })
        company_department_2.save()

        # create company employees with indexed attributes
        company_employee_1 = CompanyEmployee({
            "company_id": company_profile.id,
            "full_name": "John Doe",
            "department": "engineering",
            "title": "senior"
        })
        company_employee_1.save()

        company_employee_2 = CompanyEmployee({
            "company_id": company_profile.id,
            "full_name": "Jane Doe",
            "department": "engineering",
            "title": "senior"
        })
        company_employee_2.save()

        company_employee_3 = CompanyEmployee({
            "company_id": company_profile.id,
            "full_name": "John Smith",
            "department": "engineering",
            "title": "manager"
        })
        company_employee_3.save()

        company_employee_4 = CompanyEmployee({
            "company_id": company_profile.id,
            "full_name": "Jane Smith",
            "department": "sales",
            "title": "manager"
        })
        company_employee_4.save()

        # get all objects via table and via model
        get_all_via_table = CompanyEmployee.table.get_all(f"COMPANY#{company_profile.id}")
        get_all_via_model = CompanyProfile.get_all(company_profile.id, get_related=True)

        # create dict of company employees from engineering department
        employees_in_engineering = {
            company_employee_1.id: company_employee_1.serialize(),
            company_employee_2.id: company_employee_2.serialize(),
            company_employee_3.id: company_employee_3.serialize()
        }

        # get all company employees by department (lsi) via the table and via model
        employees_in_engineering_via_table = CompanyEmployee.table.get_all(f"COMPANY#{company_profile.id}", "engineering", index="users-by-department-index")
        employees_in_engineering_via_model = CompanyProfile.get_all(company_profile.id, "engineering", index="users-by-department-index")

        # create dict of company employees from engineering department that are software engineers
        seniors_in_engineering = {
            company_employee_1.id: company_employee_1.serialize(),
            company_employee_2.id: company_employee_2.serialize()
        }

        # get all company employees by department and title (gsi) via the table and via model
        seniors_in_engineering_via_table = CompanyEmployee.table.get_all("engineering", "senior", index="users-by-department-and-title-index")
        seniors_in_engineering_via_model = CompanyProfile.get_all("engineering", "senior", index="users-by-department-and-title-index")

        # cleanup
        company_profile.delete()
        company_department_1.delete()
        company_department_2.delete()
        company_employee_1.delete()
        company_employee_2.delete()
        company_employee_3.delete()
        company_employee_4.delete()

        # via table
        assert len(get_all_via_table) == 7

        assert len(employees_in_engineering_via_table) == 3
        for employee_via_table in employees_in_engineering_via_table:
            company_employee_id = employee_via_table.get("sk")[9:]
            assert company_employee_id in employees_in_engineering
            del employees_in_engineering[company_employee_id]

        assert len(seniors_in_engineering_via_table) == 2
        for employee_via_table in seniors_in_engineering_via_table:
            company_employee_id = employee_via_table.get("sk")[9:]
            assert company_employee_id in seniors_in_engineering
            del seniors_in_engineering[company_employee_id]

        # via model
        assert len(get_all_via_model) == 7

        assert len(employees_in_engineering_via_model) == 3
        for employee in employees_in_engineering_via_model:
            assert isinstance(employee, CompanyEmployee)

        assert len(seniors_in_engineering_via_model) == 2
        for employee in seniors_in_engineering_via_model:
            assert isinstance(employee, CompanyEmployee)

    def test_get_all_related_without_related_models_defined_failes(self):
        try:
            CompanyDepartment.get_all("some_pk", "some_sk", get_related=True)
        except RelatedModelsMissing as e:
            assert str(e) == "related_models are not defined for this object"
        except Exception:
            assert not bool("Failed to raise RelatedModelsMissing exception")
