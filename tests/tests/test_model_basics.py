import uuid
from datetime import datetime
from microgue.models.attributes import Attribute, DatetimeAttribute, ListAttribute, KeyAttribute, ObjectAttribute, ObjectListAttribute, UniqueAttribute
from microgue.models.base_table import BaseTable
from microgue.models.errors import RequiredAttributesMissing, TypeCheckFailed, UniqueAttributesExist
from microgue.models.object import Object
from microgue.models.model import Model
from unittest import TestCase


class SubSubObject(Object):
    sub_sub_attribute = Attribute(type=str)


class SubObject(Object):
    sub_attribute = Attribute(type=str)
    sub_sub_object = ObjectAttribute(type=SubSubObject)


class SubObjectWithNoDefault(Object):
    sub_attribute = Attribute(type=str)


class Child(Object):
    child_attribute = Attribute(type=str)


class User(Model):
    class Table(BaseTable):
        table_name = "microgue-users-local"
        pk = "id"
        sk = None

    id = KeyAttribute(key="id")
    basic_attribute = Attribute()
    required_attribute = Attribute(required=True)
    hidden_attribute = Attribute(hidden=True)
    default_attribute = Attribute(default=True, default_value="default_value")
    type_attribute = Attribute(type=str)
    birthday_attribute = Attribute(
        type=datetime,
        serialize=lambda x: None if x is None else x.strftime("%m-%d-%Y"),
        deserialize=lambda x: None if x is None else datetime.strptime(x, "%m-%d-%Y")
    )
    basic_list_attribute = Attribute(type=list, default=True, default_value=list)
    list_attribute = ListAttribute()
    object_list_attribute = ObjectListAttribute(item_type=Child)
    unique_attribute = UniqueAttribute()
    datetime_attribute = DatetimeAttribute()
    sub_object_attribute = ObjectAttribute(type=SubObject)
    sub_object_with_no_default_attribute = ObjectAttribute(type=SubObjectWithNoDefault, default=False)


class TestModelAttributesBasic(TestCase):
    def test_attributes(self):
        user = User()

        # check that basic attributes are accessible
        try:
            user.basic_attribute
            assert bool("Attributes are accessible")
        except:  # noqa
            assert not bool("Attributes are not accessible")

        # check that non-existent attributes are not accessible
        try:
            user.non_existent_attribute
            assert not bool("Non-existent attributes are accessible")
        except AttributeError:
            assert bool("Non-existent attributes are not accessible")

    def test_hidden_attribute(self):
        user = User({
            "hidden_attribute": "hidden_value"
        })

        assert user.serialize().get('hidden_attribute') is None
        assert user.serialize(hide_attributes=False).get('hidden_attribute') == "hidden_value"

    def test_required_attribute(self):
        user = User()

        # check that required attributes throw an error when not set
        try:
            user.save()
        except RequiredAttributesMissing as e:
            assert str(e) == "missing the following required attributes: required_attribute"

        user.required_attribute = "required_value"

        # check that required attributes do not throw an error when set
        try:
            user.save()
            assert bool("Required attributes do not throw an error when set")
        except:  # noqa
            assert not bool("Required attributes throw an error when set")

        # cleanup
        user.delete()

    def test_default_attribute(self):
        user = User()

        # check that default attributes are set
        assert user.default_attribute == "default_value"

        # check that non-default attributes are not set
        assert user.basic_attribute is None

    def test_type_attribute(self):
        user = User({
            "required_attribute": "required_value"
        })

        # check that type attributes are validated
        try:
            user.type_attribute = 1234
            user.save()
            user.delete()  # should not get here
            assert not bool("Type attributes are not validated")
        except TypeCheckFailed as e:
            assert str(e) == "the following attributes failed type checking: type_attribute"

        # check that type attributes are validated
        try:
            user.type_attribute = "str_value"
            user.save()
            assert bool("Type attributes are validated")
        except:  # noqa
            assert not bool("Failed to save type attribute")

        # cleanup
        user.delete()

    def test_serialize_and_deserialize_function_attributes(self):
        user = User()

        user.birthday_attribute = datetime.strptime("01/01/2020", "%m/%d/%Y")

        assert isinstance(user.birthday_attribute, datetime)
        assert isinstance(user.serialize().get('birthday_attribute'), str)
        assert user.serialize().get('birthday_attribute') == "01-01-2020"

    def test_datetime_attribute(self):
        user = User()

        user.datetime_attribute = datetime.strptime("01/01/2020", "%m/%d/%Y")

        assert isinstance(user.datetime_attribute, datetime)
        assert isinstance(user.serialize().get('datetime_attribute'), str)
        assert user.serialize().get('datetime_attribute') == "2020-01-01T00:00:00"

    def test_basic_list_attribute(self):
        user = User({
            "required_attribute": "required_value"
        })

        # check that the basic list attribute is defaulting to an empty list
        assert isinstance(user.basic_list_attribute, list)
        assert user.basic_list_attribute == []

        # add some values to the list
        user.basic_list_attribute.append("list_value_1")
        user.basic_list_attribute.append("list_value_2")
        user.basic_list_attribute.append("list_value_3")

        # check that the list values can be saved
        try:
            user.save()
            assert bool("Basic list attributes can be saved")
        except:  # noqa
            assert not bool("Basic list attributes cannot be saved")

        try:
            user_2 = User.get(user.id)
        except:  # noqa
            pass
        finally:
            user.delete()

        # check that the list values match on the original and retrieved user
        assert isinstance(user_2.basic_list_attribute, list)
        assert user.basic_list_attribute == user_2.basic_list_attribute

    def test_list_attribute(self):
        user = User({
            "required_attribute": "required_value"
        })

        # check that the list attribute is defaulting to an empty list
        assert isinstance(user.list_attribute, list)
        assert user.list_attribute == []

        # add some values to the list
        user.list_attribute.append("list_value_1")
        user.list_attribute.append("list_value_2")
        user.list_attribute.append("list_value_3")

        # check that the list values can be saved
        try:
            user.save()
            assert bool("List attributes can be saved")
        except:  # noqa
            assert not bool("List attributes cannot be saved")

        try:
            user_2 = User.get(user.id)
        except:  # noqa
            pass
        finally:
            user.delete()

        # check that the list values match on the original and retrieved user
        assert isinstance(user_2.list_attribute, list)
        assert user.list_attribute == user_2.list_attribute

    def test_object_list_attribute(self):
        user = User({
            "required_attribute": "required_value"
        })

        # check that the object list attribute is defaulting to an empty list
        assert isinstance(user.object_list_attribute, list)
        assert user.object_list_attribute == []

        # add some values to the list
        child_1 = Child({
            "child_attribute": "child_value_1"
        })
        child_2 = Child({
            "child_attribute": "child_value_2"
        })
        child_3 = Child({
            "child_attribute": "child_value_3"
        })

        user.object_list_attribute.append(child_1)
        user.object_list_attribute.append(child_2)
        user.object_list_attribute.append(child_3)

        # check that the list values can be saved
        try:
            user.save()
            assert bool("Object list attributes can be saved")
        except:  # noqa
            assert not bool("Object list attributes cannot be saved")

        try:
            user_2 = User.get(user.id)
        except:  # noqa
            pass
        finally:
            user.delete()

        # check all list value are Child object in user
        for child in user.object_list_attribute:
            assert isinstance(child, Child)

        # check all list values are Child objects in user_2
        for child in user_2.object_list_attribute:
            assert isinstance(child, Child)

        # check that the list values match on the original and retrieved user
        assert isinstance(user_2.object_list_attribute, list)

        # check that each child in the list is the same
        for i in range(len(user.object_list_attribute)):
            assert user.object_list_attribute[i].serialize() == user_2.object_list_attribute[i].serialize()

        # check that list values match when serialized
        assert user.serialize() == user_2.serialize()

    def test_unique_attributes(self):
        user = User({
            "required_attribute": "required_value"
        })

        user.unique_attribute = f"unique-attribute-{uuid.uuid4()}"

        # check that you can insert a unique attribute
        try:
            user.save()
            assert bool("Unique attributes can be inserted")
        except:  # noqa
            assert not bool("Unique attributes cannot be inserted")

        # check that you can save your own unique attribute as the same
        try:
            user.save()
            assert bool("Unique attributes can be saved as the same")
        except:  # noqa
            user.delete()
            assert not bool("Unique attributes cannot be saved as the same")

        # check that you can get by unique attribute
        try:
            by_unique_user = User.get_by_unique_attribute("unique_attribute", user.unique_attribute)
            assert user.serialize() == by_unique_user.serialize()
        except:  # noqa
            user.delete()

        user_2 = User({
            "required_attribute": "required_value"
        })

        user_2.unique_attribute = user.unique_attribute

        # check that a new user cannot be created with the same unique attribute as another user
        try:
            user_2.save()  # should throw an error
            user.delete()  # should not get here
            user_2.delete()  # should not get here
            assert not bool("Unique attributes are not being enforced")
        except UniqueAttributesExist as e:
            assert str(e) == "the following attributes are not unique: unique_attribute"

        user.unique_attribute = f"new-unique-attribute-{uuid.uuid4()}"

        # check that you can update your own unique attribute
        try:
            user.save()
            assert bool("Unique attributes can be updated")
        except:  # noqa
            user.delete()
            user_2.delete()
            assert not bool("Unique attributes cannot be updated")

        # check that the old unique attribute is no longer unique
        try:
            user_2.save()
            assert bool("Unique attributes can be reused")
        except:  # noqa
            user.delete()
            user_2.delete()
            assert not bool("Unique attributes cannot be reused")

        # cleanup
        user.delete()
        user_2.delete()

    def test_sub_object_attributes(self):
        user = User({
            "required_attribute": "required_value"
        })

        assert isinstance(user.sub_object_attribute, SubObject)
        assert isinstance(user.sub_object_attribute.sub_sub_object, SubSubObject)

        # check that sub objects with no default throw an error when not set
        try:
            user.sub_object_with_no_default_attribute.sub_attribute
            assert not bool("Sub objects with no default are accessible")
        except AttributeError:
            assert bool("Sub objects with no default are not accessible")

        user.sub_object_with_no_default_attribute = SubObjectWithNoDefault()
        user.sub_object_with_no_default_attribute.sub_attribute = "new_sub_value"

        user.sub_object_attribute.sub_attribute = "sub_value"

        try:
            user.save()
            assert bool("Sub object attributes can be saved")
        except:  # noqa
            user.delete()
            assert not bool("Sub object attributes cannot be saved")

        same_user = User.get(user.id)

        # cleanup
        user.delete()

        assert isinstance(same_user.sub_object_attribute, SubObject)
        assert user.serialize() == same_user.serialize()
