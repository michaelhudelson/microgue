# set local variables
locals {
  env = path_relative_to_include()
}

# configure S3 as the backend
remote_state {
  backend = "s3"
  config = {
    bucket = "microgue-state-${local.env}"
    region = "us-east-2"
    key = "terraform.tfstate"
  }
}

# define the inputs
inputs = {
  string_secret_name = "microgue-string-secret-${local.env}"
  json_secret_name = "microgue-json-secret-${local.env}"

  users_table_name = "microgue-users-${local.env}"
  single_table_name = "microgue-single-table-${local.env}"

  message_queue_name = "microgue-message-queue-${local.env}"
}
