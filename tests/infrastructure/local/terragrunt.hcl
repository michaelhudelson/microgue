# include the root terragrunt file
include { path = find_in_parent_folders() }

# include the root terraform files
terraform { source = ".." }
