provider "aws" {
  region = "us-east-2"
}

terraform {
  backend "s3" {}
}

# SECRET AS STRING
variable "string_secret_name" { type = string }
resource "aws_secretsmanager_secret" "string_secret" {
  name = var.string_secret_name
  recovery_window_in_days = 0
}
resource "aws_secretsmanager_secret_version" "string_secret_value" {
  secret_id     = aws_secretsmanager_secret.string_secret.id
  secret_string = "success"
}

# SECRET AS JSON
variable "json_secret_name" { type = string }
resource "aws_secretsmanager_secret" "json_secret" {
  name = var.json_secret_name
  recovery_window_in_days = 0
}
resource "aws_secretsmanager_secret_version" "json_secret_value" {
  secret_id     = aws_secretsmanager_secret.json_secret.id
  secret_string = "{\"username\":\"some_username\",\"password\":\"some_password\"}"
}

# DYNAMODB BASIC
variable "users_table_name" { type = string }
resource "aws_dynamodb_table" "users_table" {
  name           = var.users_table_name
  billing_mode   = "PROVISIONED"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "id"

  attribute {
    name = "id"
    type = "S"
  }
}

# DYNAMODB SINGLE TABLE
variable "single_table_name" { type = string }
resource "aws_dynamodb_table" "single_table" {
  name           = var.single_table_name
  billing_mode   = "PROVISIONED"
  read_capacity  = 1
  write_capacity = 1
  hash_key       = "pk"
  range_key      = "sk"

  attribute {
    name = "pk"
    type = "S"
  }
  attribute {
    name = "sk"
    type = "S"
  }
  attribute {
    name = "title"
    type = "S"
  }
  attribute {
    name = "department"
    type = "S"
  }

  local_secondary_index {
    name            = "users-by-department-index"
    projection_type = "ALL"
    range_key       = "department"
  }

  global_secondary_index {
    name            = "users-by-department-and-title-index"
    hash_key        = "department"
    range_key       = "title"
    projection_type = "ALL"
    read_capacity   = 1
    write_capacity  = 1
  }
}

# SQS QUEUE
variable "message_queue_name" { type = string }
resource "aws_sqs_queue" "message_queue" {
  name = var.message_queue_name
}
