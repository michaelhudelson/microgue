class ErrorConstants:
    class App:
        UNABLE_TO_AUTHENTICATE = "unable to authenticate"
        UNABLE_TO_AUTHORIZE = "unauthorized to perform operation"
        REQUESTED_URL_NOT_FOUND = "the requested url was not found on the server"
        METHOD_NOT_ALLOWED = "the method is not allowed for the requested url"
        INTERNAL_SERVER_ERROR = "internal server error"
