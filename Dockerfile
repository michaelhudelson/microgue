FROM python:3.7-slim

# define the default directory inside the container
WORKDIR /app

# install git inside the container
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y git

# copy the requirements into the container
COPY ./requirements.txt /app/requirements.txt

# install requirements
RUN pip install -r requirements.txt

# copy this project into the container
COPY . /app
