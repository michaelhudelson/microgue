boto3==1.33.13
flake8==5.0.4
flask==2.2.5
flask-classful==0.16.0
pytest==7.4.4
redis==5.0.1
requests==2.31.0
