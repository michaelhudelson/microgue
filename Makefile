.PHONY: help
help:
	clear
	@echo -------------------------------
	@echo - Makefile Target Information -
	@echo -------------------------------
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


.SILENT:
.PHONY: setup
setup: ## Setup the local infrastructure
	clear
	@echo Creating Local Infrastructure
	@echo ------------------------------
	cd ./tests/infrastructure/local; terragrunt apply -auto-approve


.PHONY: teardown
teardown: ## Teardown the local infrastructure
	clear
	@echo Destroying Local Infrastructure
	@echo -------------------------------
	cd ./tests/infrastructure/local; terragrunt destroy -auto-approve


.PHONY: test
test: ## Lint and test microgue
	clear
	@echo Linting Microgue
	@echo -------------------------------
	docker compose run --rm microgue flake8 . --show-source --statistic

	@echo Testing Microgue
	@echo -------------------------------
	docker compose run --rm microgue pytest --disable-pytest-warnings -vv -p no:cacheprovider --show-capture=no
